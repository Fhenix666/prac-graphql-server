import express from 'express';
//  Importar el middlerwer de GraphQL para Express
import graphqlHTTP from 'express-graphql';
import { schema } from './data/schema';


const app = express();

app.get('/', (req, res) => {
    res.send('Hola Mundo');
});



app.use('/graphql', graphqlHTTP({
    schema,                     // que esque ma se utiliza.
    graphiql: true              // indicando que se utiliza graphiql, en nuestro servidor
}));


app.listen(8000, () => console.log('El servidor esta listo'))

