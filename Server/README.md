# prac-graphql-server

Proyecto de practica de GraphQL con Node.

# Curso usemy

https://www.udemy.com/fullstack-react-graphql-y-apollo-de-principiante-a-experto/learn/v4/overview

# para reintallar:
nmp install

# Para iniciar proyecto (se registro un script 'start'):
npm start

Preparando Proyecto:

Installaciones base: 

# Nodemon: Para recargar proyecto en cada modificación.
# Servidor Express para NodeJs
Npm  install --save-dev nodemon express
# librerias de babael, para configurar Node con las caracteristicas de ESC6
Npm install --save-dev babel-cli babel-preset-env babel-preset-stage-0
Npm install --save express-graphql graphql     

# Herramientas de graphQL 
Npm install --save graphql-tools graphql-import   

# Instalando ORM moongus
Npm install --save mongoose
